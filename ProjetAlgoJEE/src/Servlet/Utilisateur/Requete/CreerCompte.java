package Servlet.Utilisateur.Requete;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

import BD.Utilisateur.FacadeUtilisateur;
import BD.Utilisateur.Utilisateur;
import Servlet.Util.ServletRequete;

@WebServlet("/CreerCompte")
public class CreerCompte extends ServletRequete {
	private static final long serialVersionUID = 1L;
       
	@EJB
	FacadeUtilisateur fu;

	@SuppressWarnings("unchecked")
	@Override
	public void handlePost(JSONObject in, JSONObject out, HttpSession session) {
	    String pseudo = (String)in.get("pseudo");
	    String password = (String)in.get("password");
		
		Utilisateur util = fu.createUtilisateur(pseudo, password);
		
		if(util != null) {
			session.setAttribute("util", util);
			
			out.put("retour", "true");
			out.put("idUtil", Integer.toString(util.getId()));
			out.put("pseudo", util.getPseudo());	
		} else {
			out.put("retour", "false");
		}
	}
}
