package Servlet.Utilisateur.Requete;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import BD.Article.FacadeArticle;
import BD.Utilisateur.FacadeUtilisateur;
import BD.Utilisateur.Utilisateur;

@WebServlet("/LogIn")
public class LogIn extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	FacadeArticle fa;
	@EJB
	FacadeUtilisateur fu;

    public LogIn() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

	/* Paramètre :
	 * - pseudo 
	 * - password
	 * Resultat : 
	 * - "set" la session courante
	 * Retour :
	 * - retour : true ou false
	 * - idUtil : identifiant de l'utilisateur
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pseudo = (String) request.getParameter("pseudo");
		String password = (String) request.getParameter("password");
		
		Utilisateur util = fu.getUtilisateur(pseudo);
		if(util != null && util.getPassword().equals(password)) {	
			request.getSession().setAttribute("util", util);
		}
		
		String nextJSP = "/Vue/Page/index.jsp";
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		
		request.setAttribute("facadeArticle", fa);
		request.setAttribute("facadeUtilisateur", fu);
		dispatcher.forward(request,response);
	}
}
