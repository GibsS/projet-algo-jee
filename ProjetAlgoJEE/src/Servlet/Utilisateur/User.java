package Servlet.Utilisateur;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import BD.Article.FacadeArticle;
import BD.Utilisateur.FacadeUtilisateur;
import BD.Utilisateur.Utilisateur;
import Servlet.Util.Util;

@WebServlet("/Utilisateur")
public class User extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	FacadeUtilisateur fu;
	@EJB
	FacadeArticle fa;

    public User() {
        super();
    }

    /* Paramètre :
     * - idUtil : identifiant de l'utilisateur dont on consulte la page
     * Contenu :
     * - pseudo
     * - score de l'utilisateur (a récupérer dans la façade où il faut définir une fonction de calcul du score)
     * - liste des taches disponibles si la page d'utilisateur est celle de celui qui consulte
     * - liste des taches courantes
     * - liste des taches finis
     * - liste des abonnements
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Parameter
		int id = Integer.parseInt(request.getParameter("idUtil"));
		// Vérifier si on regarde la fiche de d'autre personne où d'une autre personne
		Utilisateur util = (Utilisateur)request.getSession().getAttribute("util");

		if(util == null || util.getId() != id) {
			// consulte la page d'un autre utilisateur
			Util.dispatch(this, request, response, fu, fa, "/Vue/Page/utilisateur.jsp", 
					new String[]{"idUtil"});
		} else {
			// consulte sa propre page
			Util.dispatch(this, request, response, fu, fa, "/Vue/Page/workspace.jsp", 
					new String[]{"idUtil"});
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
