package Servlet.Article;

import javax.servlet.annotation.WebServlet;
import Servlet.Util.ServletPage;

@WebServlet("/EditionArticle")
public class EditionArticle extends ServletPage {
	private static final long serialVersionUID = 1L;

    /* Paramètre :
     * - idArticle : identifiant de l'article que l'on édite
     * - type : le type de l'article appartient à {algorithme}
     * - [idEdition : version "précédente"]
     * Contenu : 
     * - Champs d'édition de texte
     * - Champs d'édition de code
     * - Option pour submit
     */
	@Override
	public String[] getParametre() {
		return new String[]{"type", "idArticle"};
	}

	@Override
	public String getJSP() {
		return "/Vue/Page/editionArticle.jsp";
	}
}
