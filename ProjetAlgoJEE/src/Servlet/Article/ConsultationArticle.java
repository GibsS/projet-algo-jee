package Servlet.Article;

import javax.servlet.annotation.WebServlet;
import Servlet.Util.ServletPage;

@WebServlet("/ConsultationArticle")
public class ConsultationArticle extends ServletPage {
	private static final long serialVersionUID = 1L;

    /* Paramètre :
     * - idArticle : identifiant de l'article
     * - mode : appartient à {consultation, edition} : permet de définir si on veut consulter la version disponible ou
     * une version d'édition
     * - [idEdition] : identifiant de l'édition à consulter
     * - type : type de l'article appartient à {algorithme}
     * Contenu :
     * mode : consultation :
     * - le texte de l'édition courante
     * - les méta informations de l'article (date de dernière modification..)
     * - option de création d'une nouvelle édition (pour utilisateur en ligne)
     * - lien vers l'overview de l'article
     * mode : edition : 
     * - idem que pour la consultation
     * - une option de notation 
     */
	@Override
	public String[] getParametre() {
		return new String[]{"idArticle", "mode", "type"};
	}

	@Override
	public String getJSP() {
		return "/Vue/Page/consultationArticle.jsp";
	}
}
