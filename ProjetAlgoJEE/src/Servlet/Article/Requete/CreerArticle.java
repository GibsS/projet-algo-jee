package Servlet.Article.Requete;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

import BD.Article.FacadeArticle;
import BD.Article.Algorithme.Algorithme;
import BD.Utilisateur.FacadeUtilisateur;
import Servlet.Util.ServletRequete;

@WebServlet("/CreerArticle")
public class CreerArticle extends ServletRequete {
	private static final long serialVersionUID = 1L;
       
	@EJB
	FacadeArticle fa;
	@EJB
	FacadeUtilisateur fu;
	
	/* Paramètre :
	 * - nom : nom de l'article
	 * - type : type de l'article appartient à {algorithme}
	 * Résultat : 
	 * - Crée un nouvel article si le nom n'est pas déjà pris
	 * Retour :
	 * - retour : true ou false
	 * - idArticle : identifiant de l'article si créé
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void handlePost(JSONObject in, JSONObject out, HttpSession session) {
		String nom = (String)in.get("nom");
		String type = (String)in.get("type");
		
		if(type.equals("algorithme")) {
			Algorithme a = fa.createAlgorithme(nom);
			boolean succes = (a != null);
			
			if(succes) {
				out.put("retour", "true");
				out.put("idArticle", Integer.toString(a.getId()));
			} else {
				out.put("retour", "false");
			}
		}
	}
}
