package Servlet.Article.Requete;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

import BD.Utilisateur.Utilisateur;
import BD.Article.FacadeArticle;
import BD.Utilisateur.FacadeUtilisateur;
import Servlet.Util.ServletRequete;

@WebServlet("/NoterEdition")
public class NoterEdition extends ServletRequete {
	private static final long serialVersionUID = 1L;
     
	@EJB
	FacadeArticle fa;
	@EJB
	FacadeUtilisateur fu;

    /* Paramètre :
     * - idArticle : identifiant de l'article
     * - idEdition : identifiant de l'édition
     * (L'utilisateur qui note se déduit de l'utilisateur dans la session)
     * - positif : true ou false
     * Résultat :
     * - Le score de note est rajouté au score de l'édition
     * Retour :
     * - retour : true ou false
     * - score : le nouveau score
     */
	@SuppressWarnings("unchecked")
	@Override
	public void handlePost(JSONObject in, JSONObject out, HttpSession session) {
		int idArticle = ((Long)in.get("idArticle")).intValue();
		int idEdition = ((Long)in.get("idEdition")).intValue();
		boolean positif = (Boolean)in.get("positif");
		
		int nouveauScore = fu.voteEdition((Utilisateur)session.getAttribute("util"), idArticle, idEdition, positif);
		
		if(nouveauScore > 0) {
			out.put("retour", "true");
			out.put("score", nouveauScore);
		} else {
			out.put("retour", "false");
		}
	}
}
