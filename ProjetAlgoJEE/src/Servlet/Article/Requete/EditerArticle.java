package Servlet.Article.Requete;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import BD.Article.FacadeArticle;
import BD.Article.Algorithme.AlgorithmeImpl;
import BD.Utilisateur.FacadeUtilisateur;
import BD.Utilisateur.Utilisateur;
import Servlet.Util.ServletRequete;

@WebServlet("/EditerArticle")
public class EditerArticle extends ServletRequete {
	private static final long serialVersionUID = 1L;
       
	@EJB
	FacadeArticle fa;
	@EJB
	FacadeUtilisateur fu;
	
	/* Paramètre :
     * - idArticle : identifiant de l'article
     * - [idEdition] : identifiant de l'édition précédente
     * - *information de l'article* : ensemble de champ string définissant l'édition. cet ensemble dépend du type d'article
     * - type : le type d'article, appartient à {algorithme}
     * Résultat :
     * - Une édition de l'article est rajouté à l'ensemble des éditions de l'article.
     * Retour :
     * - retour : true ou false
     * - idEdition : identifiant de l'édition précédente
     */
	@SuppressWarnings("unchecked")
	@Override
	public void handlePost(JSONObject in, JSONObject out, HttpSession session) {
		int idArticle = Integer.parseInt((String)in.get("idArticle"));
		String idEditionS = (String)in.get("idEdition");
		int idEdition = -1;
		if(idEditionS != null) {
			idEdition = Integer.parseInt(idEditionS);
		}
		String type = (String)in.get("type");
		Utilisateur createur = (Utilisateur) session.getAttribute("util");
		
		if(type.equals("algorithme")) {
			String introduction = (String)in.get("introduction");
			String description = (String)in.get("description");
			List<Integer> sousAlgorithme = new ArrayList<Integer>();
			
			JSONArray ids = (JSONArray)in.get("sousAlgorithme");
			for(Object o : ids) {
				sousAlgorithme.add(((Long)((JSONObject)o).get("id")).intValue());
			}
			
			System.out.println(introduction);
			System.out.println(description);
			for(Integer i : sousAlgorithme)
				System.out.println(i);
			System.out.println(createur.getPseudo());
			
			AlgorithmeImpl ai = fa.createEditionAlgorithme(createur, idArticle, idEdition, introduction, description, sousAlgorithme);
			
			if(ai != null) {
				out.put("retour", "true");
				out.put("idEdition", Integer.toString(ai.getId()));
			} else {
				out.put("retour", "false");
			}
		}
	}
}
