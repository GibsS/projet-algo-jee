package Servlet.Article;

import javax.servlet.annotation.WebServlet;
import Servlet.Util.ServletPage;

@WebServlet("/OverviewArticle")
public class OverviewArticle extends ServletPage {
	private static final long serialVersionUID = 1L;

    /* Paramètre :
     * - idArticle : identifiant de l'article à "consulter"
     * - type : type de l'article
     * Contenu :
     * - Lien à l'édition de consultation ("/ConsultationArticle blabla") + information relative à sa mise
     * en ligne (date de passage en ligne)
     * - Liste des éditions courantes
     * - Liste de taches à récupérer
     * - Liste de taches en cours + utilisateur qui ont récupéré
     * - Liste de taches finis en cours de notation
     * - Liste de taches archivés
     */
	@Override
	public String[] getParametre() {
		return new String[]{"idArticle", "type"};
	}

	@Override
	public String getJSP() {
		return "/Vue/Page/overviewArticle.jsp";
	}
}
