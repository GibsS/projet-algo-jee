package Servlet.Tache.Requete;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

import BD.Article.FacadeArticle;
import BD.Utilisateur.FacadeUtilisateur;
import BD.Utilisateur.Tache;
import BD.Utilisateur.Utilisateur;
import Servlet.Util.ServletRequete;

@WebServlet("/NoterTache")
public class NoterTache extends ServletRequete {
	private static final long serialVersionUID = 1L;
       
	@EJB
	FacadeUtilisateur fu;
	@EJB
	FacadeArticle fa;

    /* Paramètre :
     * - idTache
     * Résultat :
     * - score changé pour la tache
     * Retour :
     * - retour : true ou false
     * - score : le nouveau score
     */
	@SuppressWarnings("unchecked")
	@Override
	public void handlePost(JSONObject in, JSONObject out, HttpSession session) {
		int idTache = ((Long)in.get("idTache")).intValue();
		boolean positif = (Boolean)in.get("positif");
		
		Utilisateur utilisateur = (Utilisateur)session.getAttribute("util");
		
		Tache tache = fu.getTache(idTache);
		
		int score = fu.voteTache(utilisateur, tache, positif);
		
		if(score > 0) {		
			out.put("score", Integer.toString(score));
			out.put("retour", "true");
		} else {
			out.put("retour", "false");
		}
	}
}
