package Servlet.Tache.Requete;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

import BD.Article.FacadeArticle;
import BD.Utilisateur.FacadeUtilisateur;
import BD.Utilisateur.Tache;
import BD.Utilisateur.Utilisateur;
import Servlet.Util.ServletRequete;

@WebServlet("/RecupererTache")
public class RecupererTache extends ServletRequete {
	private static final long serialVersionUID = 1L;
       
	@EJB
	FacadeUtilisateur fu;
	@EJB
	FacadeArticle fa;

    /* Paramètre :
     * - idTache : identifiant de la tache
     * Résultat : 
     * - changement de status de la tache
     * Retour :
     * - true ou false
     */
	@SuppressWarnings("unchecked")
	@Override
	public void handlePost(JSONObject in, JSONObject out, HttpSession session) {
		int idTache = Integer.parseInt((String)in.get("idTache"));
		
		Utilisateur utilisateur = (Utilisateur)session.getAttribute("util");
		
		Tache tache = fu.getTache(idTache);
		
		if(fu.recupererTache(utilisateur, tache)) {		
			out.put("retour", "true");
		} else {
			out.put("retour", "false");
		}
	}
}
