package Servlet.Tache.Requete;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import BD.Article.Article;
import BD.Article.FacadeArticle;
import BD.Utilisateur.FacadeUtilisateur;
import BD.Utilisateur.Tache;
import BD.Utilisateur.Utilisateur;
import Servlet.Util.ServletRequete;

@WebServlet("/CreerTache")
public class CreerTache extends ServletRequete {
	private static final long serialVersionUID = 1L;
       
	@EJB
	FacadeUtilisateur fu;
	@EJB
	FacadeArticle fa;
	
    /* Paramètre :
     * - nom 
     * - description
     * - article[] : identifiant du premier article lié
     * (le créateur se déduit de la session)
     * Résultat :
     * - une tache est rajouté en attente de récupération 
     * Retour :
     * - retour : true ou false
     * - idTache : identifiant de la tache
     */
	@SuppressWarnings("unchecked")
	@Override
	public void handlePost(JSONObject in, JSONObject out, HttpSession session) {
		String nom = (String)in.get("nom");
		String description = (String)in.get("description");
		List<Article> articles = new ArrayList<Article>();
		
		Utilisateur utilisateur = (Utilisateur)session.getAttribute("util");
		
		JSONArray ids = (JSONArray)in.get("article");
		for(Object o : ids) {
			articles.add(fa.getArticle(((Long)((JSONObject)o).get("id")).intValue()));
		}
		
		Tache t = fu.createTache(utilisateur, nom, description, articles);

		if(t != null) {
			out.put("retour", "true");
			out.put("idTache", Integer.toString(t.getId()));
		} else {
			out.put("retour", "false");
		}
	}
}
