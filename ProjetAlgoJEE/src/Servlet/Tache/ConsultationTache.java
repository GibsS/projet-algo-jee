package Servlet.Tache;

import javax.servlet.annotation.WebServlet;
import Servlet.Util.ServletPage;

@WebServlet("/ConsultationTache")
public class ConsultationTache extends ServletPage {
	private static final long serialVersionUID = 1L;

    /* Paramètre :
     * - idTache : l'identifiant de la tache à regarder
     * Contenu :
     * - nom
     * - description
     * - option de récupération si il est disponible
     * - option de notation si il est fini
     */
	@Override
	public String[] getParametre() {
		return new String[]{"idTache"};
	}

	@Override
	public String getJSP() {
		return "/Vue/Page/consultationTache.jsp";
	}
}
