package Servlet.Tache;

import javax.servlet.annotation.WebServlet;
import Servlet.Util.ServletPage;

@WebServlet("/CreationTache")
public class CreationTache extends ServletPage {
	private static final long serialVersionUID = 1L;
	
    /* Paramètre :
     * Aucun
     * Contenu : 
     * - Champ nom
     * - Champ description
     * - Application d'ajout d'article à référencer
     */
	@Override
	public String[] getParametre() {
		return new String[]{};
	}

	@Override
	public String getJSP() {
		return "/Vue/Page/editionTache.jsp";
	}
}
