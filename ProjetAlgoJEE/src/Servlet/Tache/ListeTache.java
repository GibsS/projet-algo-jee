package Servlet.Tache;

import javax.servlet.annotation.WebServlet;
import Servlet.Util.ServletPage;

@WebServlet("/ListeTache")
public class ListeTache extends ServletPage {
	private static final long serialVersionUID = 1L;
    
    /* Paramètre :
     * aucun :
     * Contenu :
     * Une liste exhaustive des taches 
     */
	@Override
	public String[] getParametre() {
		return new String[]{};
	}

	@Override
	public String getJSP() {
		return "/Vue/Page/listeTache.jsp";
	}
}
