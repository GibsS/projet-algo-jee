package Servlet.General;

import javax.servlet.annotation.WebServlet;
import Servlet.Util.ServletPage;

@WebServlet("/Recherche")
public class Recherche extends ServletPage {
	private static final long serialVersionUID = 1L;

    /* Paramètre :
     * - query : le string a traité contenant les informations à parser
     * - page : numéro de la page
     * - compte : le nombre d'éléments à afficher
     * - algorithme : true ou false
     * Contenu :
     * - Une application de recherche
     * - La liste de recherche
     * - option de changement de page
     */
	@Override
	public String[] getParametre() {
		return new String[]{"query", "page", "compte", "algorithme"};
	}

	@Override
	public String getJSP() {
		return "/Vue/Page/recherche.jsp";
	}
}
