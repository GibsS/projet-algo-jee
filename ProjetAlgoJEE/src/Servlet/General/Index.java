package Servlet.General;

import javax.servlet.annotation.WebServlet;
import Servlet.Util.ServletPage;

@WebServlet("/")
public class Index extends ServletPage {
	private static final long serialVersionUID = 1L;
    
	/* Paramètre :
     * Aucun
     * Contenu :
     * - Message de bienvenu (?)
     * - Application de "sign up"
     */
	@Override
	public String[] getParametre() {
		return new String[]{};
	}

	@Override
	public String getJSP() {
		return "/Vue/Page/index.jsp";
	}
}
