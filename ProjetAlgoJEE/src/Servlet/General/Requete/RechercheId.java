package Servlet.General.Requete;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import BD.Article.Article;
import BD.Article.FacadeArticle;
import BD.Article.Algorithme.Algorithme;
import BD.Utilisateur.FacadeUtilisateur;
import Servlet.Util.ServletRequete;

@WebServlet("/RechercheId")
public class RechercheId extends ServletRequete {
	private static final long serialVersionUID = 1L;
       
	@EJB
	FacadeArticle fa;
	@EJB
	FacadeUtilisateur fu;

	/* Paramètre :
     * - query : string à parser pour obtenir des articles dont le nom s'approche
     * - algorithme : comprendre les algorithme?
     * Résultat :
     * - Aucune modification
     * Retour :
     * - resultat : un tableau d'élément comprenant :
     * 		- nom : nom de l'article
     * 		- type : le type de l'article 
     * 		- id : l'identifiant de l'article
     */
	@SuppressWarnings("unchecked")
	@Override
	public void handlePost(JSONObject in, JSONObject out, HttpSession session) {
		String query = (String)in.get("query");
		if(query == null)
			query = "";
		boolean algorithme = (Boolean)in.get("algorithme");
		
		List<Article> article = fa.getArticles(query, algorithme);
			
		JSONArray array = new JSONArray();
		for(Article a : article) {
			if(a instanceof Algorithme) {
				JSONObject o = new JSONObject();
				o.put("nom", a.getNom());
				o.put("type", "algorithme");
				o.put("id", a.getId());
				array.add(o);
			}
		}
		out.put("resultat", array);	
	}

}
