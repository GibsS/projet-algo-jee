package Servlet.Util;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import BD.Article.FacadeArticle;
import BD.Utilisateur.FacadeUtilisateur;

public abstract class ServletPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	FacadeArticle fa;
	@EJB
	FacadeUtilisateur fu;

	public ServletPage() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Util.dispatch(this, request, response, fu, fa, getJSP(), getParametre());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
	
	public abstract String[] getParametre();
	public abstract String getJSP();
}
