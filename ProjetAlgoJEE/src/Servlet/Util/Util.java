package Servlet.Util;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import BD.Article.FacadeArticle;
import BD.Utilisateur.FacadeUtilisateur;

public class Util {

	public static JSONObject getJSONObject(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();
	    try 
	    {
	      BufferedReader reader = request.getReader();
	      String line = null;
	      while ((line = reader.readLine()) != null)
	      {
	        sb.append(line);
	      }
	    } catch (Exception e) { e.printStackTrace(); }
	 
	    JSONParser parser = new JSONParser();
	    JSONObject data = null;
	    try
	    {
	      data = (JSONObject) parser.parse(sb.toString());
	    } catch (ParseException e) { e.printStackTrace(); }
	    return data;
	}
	
	public static void dispatch(HttpServlet s, HttpServletRequest request, HttpServletResponse response, FacadeUtilisateur fu, FacadeArticle fa, String jspURI, String[] param) throws ServletException, IOException{
		for(String p : param)
			testParameter(request, p);
		
		RequestDispatcher dispatcher = s.getServletContext().getRequestDispatcher(jspURI);
		
		request.setAttribute("facadeArticle", fa);
		request.setAttribute("facadeUtilisateur", fu);
		dispatcher.forward(request,response);
	}
	public static void testParameter(HttpServletRequest request, String param) {
		if(request.getParameter(param) == null) {
			throw new RuntimeException(param + " is undefined..");
		}
	}
}
