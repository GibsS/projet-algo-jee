package Servlet.Util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

public abstract class ServletRequete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ServletRequete() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject in = Util.getJSONObject(request);
		JSONObject out = new JSONObject();
		
		handlePost(in, out, request.getSession());
		
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter outPrint = response.getWriter();
		outPrint.print(out.toString());
	}

	public abstract void handlePost(JSONObject in, JSONObject out, HttpSession session);
}
