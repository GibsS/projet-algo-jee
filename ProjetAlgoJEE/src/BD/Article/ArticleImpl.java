package BD.Article;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import BD.Utilisateur.Utilisateur;

@Entity
public abstract class ArticleImpl {

	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	int id;
	
	int etat; // 0 : en attente de validation; 1 : archivé; 2 : consultation
	
	// Lien entre édition
	@ManyToOne
	protected ArticleImpl precedent;
	@OneToMany
	protected Set<ArticleImpl> suivants;
	
	// Information de commit
	@ManyToOne
	Utilisateur createur;
	String commentaire;
	
	@ManyToMany 
	Set<Utilisateur> noteurs;
	
	int score;
	
	public abstract void commitDiff();
	
	public abstract Article getArticle();

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEtat() {
		return etat;
	}
	public void setEtat(int etat) {
		this.etat = etat;
	}
	public ArticleImpl getPrecedent() {
		return precedent;
	}
	public void setPrecedent(ArticleImpl precedent) {
		this.precedent = precedent;
	}
	public Set<ArticleImpl> getSuivants() {
		return suivants;
	}
	public void setSuivants(Set<ArticleImpl> suivants) {
		this.suivants = suivants;
	}
	public Utilisateur getCreateur() {
		return createur;
	}
	public void setCreateur(Utilisateur createur) {
		this.createur = createur;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	public Set<Utilisateur> getNoteurs() {
		return noteurs;
	}
	public void setNoteurs(Set<Utilisateur> noteurs) {
		this.noteurs = noteurs;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
}
