package BD.Article;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import BD.Utilisateur.Tache;
import BD.Utilisateur.Utilisateur;

@Entity
public abstract class Article {

	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	int id;
	
	String nom;
	
	int type; // 0 : Algorithme
	
	@ManyToMany(fetch=FetchType.EAGER)
	Set<Tache> requetesTache;
	
	@ManyToMany(fetch=FetchType.EAGER)
	Set<Utilisateur> abonnes;
	
	int score;
	
	public abstract ArticleImpl getConsultable();
	public abstract void setConsultable(ArticleImpl ai);
	public abstract Set<ArticleImpl> getEdition();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Set<Tache> getRequetesTache() {
		return requetesTache;
	}
	public void setRequetesTache(Set<Tache> requetesTache) {
		this.requetesTache = requetesTache;
	}
	public Set<Utilisateur> getAbonnes() {
		return abonnes;
	}
	public void setAbonnes(Set<Utilisateur> abonnes) {
		this.abonnes = abonnes;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
}
