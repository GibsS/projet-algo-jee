package BD.Article;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import BD.Article.Algorithme.Algorithme;
import BD.Article.Algorithme.AlgorithmeImpl;
import BD.Utilisateur.Utilisateur;

@Singleton
public class FacadeArticle {

	@PersistenceContext(name="MaPU")
	EntityManager em;
	
	public void init() {
		Utilisateur u = new Utilisateur();
		u.setPseudo("Emerick");
		u.setPassword("emerick");
		
		Algorithme b = new Algorithme("Djikstra");
		
		Algorithme a = new Algorithme("A *");
		AlgorithmeImpl ai = new AlgorithmeImpl();
		ai.setDescription("<p>A* commence à un nœud choisi. Il applique à ce nœud un « coût » (habituellement zéro pour le nœud initial). A* estime ensuite la distance qui sépare ce nœud du but à atteindre. La somme du coût et de l'évaluation représente le coût heuristique assigné au chemin menant à ce nœud. Le nœud est alors ajouté à une file d'attente prioritaire, couramment appelée open list.L'algorithme retire le premier nœud de la file d'attente prioritaire (dû au fonctionnement d'une file d'attente, le nœud à l'heuristique la plus basse est retiré en premier). Si la file d'attente est vide, il n'y a aucun chemin du nœud initial au nœud d'arrivée, ce qui interrompt l'algorithme. Si le nœud retenu est le nœud d'arrivée, A* reconstruit le chemin complet et s'arrête. Pour cette reconstruction on se sert d'une partie des informations sauvées dans la liste communément appelé closed list décrite plus bas."
				+ "</p><p>Si le nœud n'est pas le nœud d'arrivée, de nouveaux nœuds sont créés pour tous les nœuds contigus admissibles ; la manière exacte de faire dépend du problème à traiter. Pour chaque nœud successif, A* calcule son coût et le stocke avec le nœud. Ce coût est calculé à partir de la somme du coût de son ancêtre et du coût de l'opération pour atteindre ce nouveau nœud."
				+ "</p><p>L'algorithme maintient également la liste de nœuds qui ont été vérifiés, couramment appelée closed list. Si un nœud nouvellement produit est déjà dans cette liste avec un coût égal ou inférieur, aucune opération n'est faite sur ce nœud ni sur son homologue se trouvant dans la liste."
				+ "</p><p>Après, l'évaluation de la distance du nouveau nœud au nœud d'arrivée est ajoutée au coût pour former l'heuristique du nœud. Ce nœud est alors ajouté à la liste d'attente prioritaire, à moins qu'un nœud identique dans cette liste ne possède déjà une heuristique inférieure ou égale."
				+ "</p><p>Une fois les trois étapes ci-dessus réalisées pour chaque nouveau nœud contigu, le nœud original pris de la file d'attente prioritaire est ajouté à la liste des nœuds vérifiés. Le prochain nœud est alors retiré de la file d'attente prioritaire et le processus recommence."
				+ "</p><p>Les deux structures open list et closed list ne sont pas nécessaires si on peut garantir que le premier chemin produit à n'importe quel nœud est le plus court. Cette situation surgit si l'heuristique est non seulement admissible mais aussi « monotone », signifiant que la différence entre l'heuristique de deux nœuds quelconques reliés ne surestime pas la distance réelle entre ces nœuds. Ce n'est possible que dans de très rares cas.</p>");
		ai.setIntroduction("Algorithme de recherche A* (qui se prononce A étoile, ou A star à l'anglaise) est un algorithme de recherche de chemin dans un graphe entre un nœud initial et un nœud final tous deux donnés. De par sa simplicité il est souvent présenté comme exemple typique d'algorithme de planification, domaine de l'intelligence artificielle. L'algorithme A* a été créé pour que la première solution trouvée soit l'une des meilleures, c'est pourquoi il est célèbre dans des applications comme les jeux vidéo privilégiant la vitesse de calcul sur l'exactitude des résultats. Cet algorithme a été proposé pour la première fois par Peter E. Hart (en), Nils John Nilsson (en) et Bertram Raphael (en) en 19681. Il s'agit d'une extension de l'algorithme de Dijkstra de 1959.");
		ai.setScore(4);
		ai.setCreateur(u);
		Set<Algorithme> sous = new HashSet<Algorithme>();
		sous.add(b);
		ai.setSousAlgorithme(sous);
		
		a.setVersionConsultation(ai);
		
		Set<ArticleImpl> editions = new HashSet<ArticleImpl>();
		editions.add(ai);
		u.setEditionsFini(editions);
		
		em.persist(u);
		em.persist(a);
		em.persist(ai);
		em.persist(b);
	}
	
	public Algorithme createAlgorithme(String nom) {
		if(nom != null && nom != "") {
			List<Article> algos = new ArrayList<Article>();
			algos.addAll(em.createQuery("from Algorithme a where a.nom='" + nom + "'", Algorithme.class).getResultList());
			
			if(algos.isEmpty()) {
				Algorithme a = new Algorithme(nom);
				em.persist(a);
				return a;
			}
		}
		return null;
	}
	public AlgorithmeImpl createEditionAlgorithme(Utilisateur createur, int idAlgorithme, int idEditionPrecedente, String introduction, 
												String description, List<Integer> sousAlgorithme) {
		// Récupération de référence
		Algorithme algo = (Algorithme)getArticle(idAlgorithme);
		/*AlgorithmeImpl algoImplPrec = null;
		if(idEditionPrecedente != -1) 
			algoImplPrec = (AlgorithmeImpl)getArticleImpl(idEditionPrecedente);*/
		
		if(algo == null)
			return null;
		
		// Création
		AlgorithmeImpl algoImpl = new AlgorithmeImpl();
		em.persist(algoImpl);
		
		algoImpl.setDescription(description);
		algoImpl.setIntroduction(introduction);
		for(Integer i : sousAlgorithme) {
			Algorithme a= (Algorithme) this.getArticle(i);
			a.getSurAlgorithme().add(algoImpl);
			em.merge(a);
		}
		
		//createur.getEditionsFini().add(algoImpl);
		//em.merge(createur);
		
		algo.getEdition().add(algoImpl);
		em.merge(algo);
		em.merge(algoImpl);
		
		return algoImpl;
	}
	public void removeAlgorithme(Algorithme a) {
		for(ArticleImpl a1 : a.getEdition())
			em.remove(em.merge(a1));
		em.remove(em.merge(a.getConsultable()));
		em.remove(em.merge(a));
	}
	
	public Article getArticle(int id) {
		List<Article> algos = new ArrayList<Article>();
		algos.addAll(em.createQuery("from Algorithme a where a.id=" + id, Algorithme.class).getResultList());
		
		if(!algos.isEmpty()) {
			return algos.get(0);
		} else {
			return null;
		}
	}
	public ArticleImpl getArticleImpl(int id) {
		List<ArticleImpl> algos = new ArrayList<ArticleImpl>();
		algos.addAll(em.createQuery("from AlgorithmeImpl a where a.id=" + id, AlgorithmeImpl.class).getResultList());
		
		if(!algos.isEmpty()) {
			return algos.get(0);
		} else {
			return null;
		}
	}
	
	// Permet de chercher les listes qui répondent au query
	public List<Article> getArticles(String query, boolean acceptAlgorithme) {
		String[] mots = query.split(" ");
		List<Article> articles = new ArrayList<Article>();
		
		if(acceptAlgorithme) {
			String requete = "from Algorithme a where a.nom like ";
			for(String word : mots) {
				requete += "'%" + word +"%'";
			}
			articles.addAll((List<Algorithme>) em.createQuery(requete, Algorithme.class).getResultList());
		}
		return articles;
	}
}
