package BD.Article.Algorithme;

import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import BD.Article.Article;
import BD.Article.ArticleImpl;

@Entity
public class AlgorithmeImpl extends ArticleImpl {

	@ManyToOne
	Algorithme articleEditant;
	
	@OneToOne
	Algorithme articleConsultant;
	
	@ManyToMany(fetch=FetchType.EAGER)
	Set<Algorithme> sousAlgorithme;
	
	@Lob
	String introduction;
	@Lob
	String description;
	
	public AlgorithmeImpl () {
		
	}
	
	public AlgorithmeImpl(String introduction, String description) {
		this.introduction = introduction;
		this.description = description;
	}
	
	public void commitDiff() {
		
	}
	
	public Article getArticle() {
		return articleEditant==null?articleConsultant:articleEditant;
	}
	
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void addSousAlgorithme(Algorithme a) {
		sousAlgorithme.add(a);
	}
	public void setSousAlgorithme(Set<Algorithme> sous) {
		sousAlgorithme = sous;
	}
	public Set<Algorithme> getSousAlgorithme() {
		return sousAlgorithme;
	}
}
