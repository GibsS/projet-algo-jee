package BD.Article.Algorithme;

import java.util.Collection;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import BD.Article.Article;
import BD.Article.ArticleImpl;

@Entity
public class Algorithme extends Article {
	
	@OneToMany(fetch=FetchType.EAGER)
	Set<ArticleImpl> versionEditions;
	
	@OneToOne
	AlgorithmeImpl versionConsultation;
	
	@ManyToMany(fetch=FetchType.EAGER, mappedBy="sousAlgorithme")
	Set<AlgorithmeImpl> surAlgorithme;

	public Algorithme() {
		
	}
	
	public Algorithme(String nom) {
		setNom(nom);
	}
	
	// Consultation et edition
	/*public AlgorithmeImpl createConsultable(EntityManager em) {
		if(versionConsultation != null) {
			versionEditions.add(versionConsultation);
			versionConsultation = null;
		}
		AlgorithmeImpl a = new AlgorithmeImpl();
		versionConsultation = a;
		em.persist(a);
		return a;
	}
	public ArticleImpl createEdition(EntityManager em, ArticleImpl a) {
		AlgorithmeImpl a1 = new AlgorithmeImpl();
		versionEditions.add(a1);
		em.persist(a1);
		return a1;
	}
	public void passToConsultable(ArticleImpl a) {
		if(versionConsultation != null) {
			versionEditions.add(versionConsultation);
			versionConsultation = null;
		}
		versionConsultation = (AlgorithmeImpl) a;
	}*/
	
	public void setConsultable(ArticleImpl ai) {
		this.versionConsultation = (AlgorithmeImpl) ai;
	}
	
	public ArticleImpl getConsultable() {
		return versionConsultation;
	}
	public Set<ArticleImpl> getEdition() {
		return versionEditions;
	}
	
	// GETTER ET SETTER
	public void setVersionConsultation(AlgorithmeImpl versionConsultation) {
		this.versionConsultation = versionConsultation;
	}
	public Collection<AlgorithmeImpl> getSurAlgorithme() {
		return surAlgorithme;
	}
	public void setSurAlgorithme(Set<AlgorithmeImpl> surAlgorithme) {
		this.surAlgorithme = surAlgorithme;
	}
}
