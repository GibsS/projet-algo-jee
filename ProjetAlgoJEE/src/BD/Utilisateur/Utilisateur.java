package BD.Utilisateur;

import java.util.Set;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import BD.Article.Article;
import BD.Article.ArticleImpl;

@Entity
public class Utilisateur {
	
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	int id;
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="createur")
	Set<Tache> tachesCree;
	@OneToMany(fetch=FetchType.EAGER, mappedBy="executeur")
	Set<Tache> tachesFini;
	@ManyToMany(fetch=FetchType.EAGER, mappedBy="noteurs")
	Set<Tache> tachesNote;
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="createur")
	Set<ArticleImpl> editionsFini; 
	@ManyToMany(fetch=FetchType.EAGER, mappedBy="noteurs")
	Set<ArticleImpl> editionsNote;
	@ManyToMany(fetch=FetchType.EAGER, mappedBy="abonnes")
	Set<Article> abonnements;
	
	String password;
	String pseudo;
	
	// GETTER ET SETTER
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public Collection<Tache> getTachesFini() {
		return tachesFini;
	}
	public Collection<ArticleImpl> getEditionsFini() {
		return editionsFini;
	}
	public Set<Tache> getTachesNote() {
		return tachesNote;
	}
	public void setTachesNote(Set<Tache> tachesNote) {
		this.tachesNote = tachesNote;
	}
	public Set<ArticleImpl> getEditionsNote() {
		return editionsNote;
	}
	public void setEditionsNote(Set<ArticleImpl> editionsNote) {
		this.editionsNote = editionsNote;
	}
	public Set<Article> getAbonnements() {
		return abonnements;
	}
	public void setAbonnements(Set<Article> abonnements) {
		this.abonnements = abonnements;
	}
	public void setTachesFini(Set<Tache> tachesFini) {
		this.tachesFini = tachesFini;
	}
	public void setEditionsFini(Set<ArticleImpl> editionsFini) {
		this.editionsFini = editionsFini;
	}
}
