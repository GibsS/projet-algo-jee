package BD.Utilisateur;

import java.util.List;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import BD.Article.Article;
import BD.Article.ArticleImpl;
import BD.Article.FacadeArticle;

@Singleton
public class FacadeUtilisateur {

	@PersistenceContext(name="MaPU")
	EntityManager em;
	
	@EJB
	FacadeArticle fa;
	
	// Utilisateur :
	public Utilisateur createUtilisateur(String pseudo, String password) {
		if(pseudo != null && password != null) {
			Utilisateur util = getUtilisateur(pseudo);
			if(util == null) {
				Utilisateur u = new Utilisateur();
				u.setPassword(password);
				u.setPseudo(pseudo);
				em.persist(u);
				return u;
			}
		} 
		return null;
	}
	public Utilisateur getUtilisateur(String pseudo) {
		List<Utilisateur> u = em.createQuery("from Utilisateur u where u.pseudo='" + pseudo + "'",
				   Utilisateur.class).getResultList();
		if(u.isEmpty()) {
			return null;
		} else {
			return u.get(0);
		}
	}
	public Utilisateur getUtilisateur(int id) {
		List<Utilisateur> u = em.createQuery("from Utilisateur u where u.id=" + id,
				   Utilisateur.class).getResultList();
		if(u.isEmpty()) {
			return null;
		} else {
			return u.get(0);
		}
	}
	public List<Utilisateur> getUtilisateurs() {
		return em.createQuery("from Utilisateur", Utilisateur.class).getResultList();
	}
	
	public int getScore(Utilisateur u) {
		return 0;
	}
	public List<Article> getAbonnements(Utilisateur u) {
		List<Article> l = new ArrayList<Article>();
		l.addAll(u.getAbonnements());
		return l;
	}
	public List<Tache> getTacheEnCours(Utilisateur u) {
		List<Tache> l = new ArrayList<Tache>();
		for(Tache t : u.getTachesFini()) {
			if(t.etat == Tache.inProgress) {
				l.add(t);
			}
		}
		return l;
	}
	
	@SuppressWarnings("unused")
	public int voteEdition(Utilisateur util, int idArticle, int idEdition, boolean positif) {
		ArticleImpl ai = fa.getArticleImpl(idEdition);
		//Utilisateur createur = ai.getCreateur();
		
		if(true/*createur.getId() == util.getId() && !ai.getNoteurs().contains(util)*/) {
			int score = ai.getScore();
			score ++;
			ai.getNoteurs().add(util);
			ai.setScore(score);
			
			if(ai.getScore() > 0) {
				Article a = fa.getArticle(idArticle);
				a.getEdition().clear();
				a.setConsultable(ai);
				em.merge(a);
			}
			em.merge(ai);
			return score;
		}
		return -1;
	}
	
	public Tache createTache(Utilisateur u, String nom, String description, List<Article> articles) {
		Tache t = new Tache();
		em.persist(t);
		
		t.setCreateur(u);
		t.setNom(nom);
		t.setDescription(description);
		for(Article a : articles) {
			a.getRequetesTache().add(t);
			em.merge(a);
		}
		t.setEtat(Tache.ready);
		em.merge(t);
		
		return t;
	}
	public void addArticleAssocie(Tache t, Article a) {
		t.addArticle(a);
		em.merge(t);
	}
	public Tache getTache(int id) {
		List<Tache> u = em.createQuery("from Tache t where t.id=" + id,
				   						Tache.class).getResultList();
		if(u.isEmpty()) {
			return null;
		} else {
			return u.get(0);
		}
	}
	public List<Tache> getTaches() {
		return em.createQuery("from Tache", Tache.class).getResultList();
	}
	public int voteTache(Utilisateur u, Tache t, boolean positif) {
		int score = -1;
		if(!t.noteurs.contains(u) && t.executeur != u && t.getEtat() == Tache.done) {
			t.addNoteur(u);
			score = t.getScore();
			score++;
			t.setScore(score);
			
			if(t.getScore() > 0) {
				t.setEtat(Tache.archived);
			}
			em.persist(t);
		}
		return score;
	}
	public boolean recupererTache(Utilisateur u, Tache t) {
		if(t.getEtat() == Tache.ready) {
			t.setExecuteur(u);
			t.setEtat(Tache.inProgress);
			em.persist(t);
			return true;
		} else {
			return false;
		}
	}
	public boolean finirTache(Utilisateur u, Tache t) {
		if(u == t.getCreateur() && t.getEtat() == Tache.inProgress) {
			t.setEtat(Tache.done);
			em.merge(t);
			return true;
		} else {
			return false;
		}
	}
}