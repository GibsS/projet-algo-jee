package BD.Utilisateur;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import BD.Article.Article;

@Entity
public class Tache {

	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	int id;
	
	@ManyToMany(mappedBy="requetesTache", fetch=FetchType.EAGER)
	Set<Article> articles = new HashSet<Article>();
	
	@ManyToOne(fetch=FetchType.EAGER)
	Utilisateur createur;
	
	@ManyToOne(fetch=FetchType.EAGER)
	Utilisateur executeur;
	
	@ManyToMany(fetch=FetchType.EAGER)
	Set<Utilisateur> noteurs; 
	
	Date creation;
	Date archivage;
	int etat; // 0 : posé; 1 : récupéré; 2 : fini; 3 : archivé
	
	public final static int ready = 0;
	public final static int inProgress = 1;
	public final static int done = 2;
	public final static int archived = 3;
	
	int score;
	
	String nom;
	String description;
	
	// GETTER ET SETTER
	public int getId() {
		return id;
	}
	public String getNom() {
		return nom;
	}
	public String getDescription() {
		return description;
	}
	public int getEtat() {
		return etat;
	}
	public Set<Article> getArticle() {
		return articles;
	}
	public Utilisateur getCreateur() {
		return createur;
	}
	public Utilisateur getExecuteur() {
		return executeur;
	}
	public Set<Utilisateur> getNoteurs() {
		return noteurs;
	}
	public int getScore() {
		return score;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setEtat(int etat) {
		this.etat = etat;
	}
	public void addArticle(Article a) {
		articles.add(a);
	}
	public void setCreateur(Utilisateur u) {
		createur = u;
	}
	public void setExecuteur(Utilisateur u) {
		executeur = u;
	}
	public void addNoteur(Utilisateur u) {
		noteurs.add(u);
	}
	public void setScore(int i) {
		score = i;
	}
}