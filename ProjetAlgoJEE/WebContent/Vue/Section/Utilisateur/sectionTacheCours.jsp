<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    
    import="BD.Utilisateur.*"
    import="BD.Article.*"
    import="BD.Article.Algorithme.*"
    import="java.util.List"
    import="java.util.ArrayList"%>

<% 	Utilisateur utilisateur = (Utilisateur)request.getAttribute("idUtil");
	FacadeUtilisateur fu = (FacadeUtilisateur)request.getAttribute("facadeUtilisateur");
	FacadeArticle fa = (FacadeArticle)request.getAttribute("facadeArticle");
	List<Tache> taches = fu.getTacheEnCours(utilisateur);%>

<h2 id="sectionTacheCours">Tache en cours <small><%=taches.size() %></small></h2>
<% for(Tache tache : taches) { %>
	<h3> <a href=<%="/ProjetAlgoJEE/Tache?id=" + tache.getId()%>><%= tache.getNom() %></a></h3>
	<p> <%= tache.getDescription() %> </p>
<% } %>
