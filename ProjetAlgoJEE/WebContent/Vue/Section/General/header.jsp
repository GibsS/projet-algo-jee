<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    
    import="BD.Utilisateur.*"%>
 
<% Utilisateur util = (Utilisateur)session.getAttribute("util"); %>

	<header>
		<div class="container">
			<a id="home_link" href="http://localhost:8080/ProjetAlgoJEE/"><img id="main_logo" src="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/images/logo_small.png" alt="mustalgo_logo"></a>

			<div id="space_middle_header">
			</div>

          	<% if(util == null) { %>
            <form class="header_form_container" action="/ProjetAlgoJEE/LogIn" method="post">
				<div class="header_form_centerer"></div>
        		<div>
          			<input class="username_input" name="pseudo" type="text" placeholder="Username">
          			<input class="password_input" name="password" type="password" placeholder="Password">
        			<button id="login_button" class="log_button" type="submit" >Log in</button>
        		</div>
				<div class="header_form_centerer"></div>
      		</form>
            <% } else { %>
            <a class="header_button" href=<% out.println("/ProjetAlgoJEE/Utilisateur?idUtil=" + util.getId()); %>><p>Bonjour, <%= util.getPseudo()%></p></a>
            <% } %> 
	
			<% if(util != null) { %>
			<div id="createurArticle" class="header_button dropdown_button" tabindex="1" ng-controller="ctrCreateurArticle">
				<p>Créer article</p>
				<ul class="header_button dropdown_menu">
					<li id="algo_name_input"><input type="text" placeholder="Name" ng-model="nomCreationArticle"/></li>
					<li><a href="" ng-click="algorithme()">Algorithme</a></li>
				</ul>
			</div>
			<script>
			var appSignUp = angular.module("createurArticle", []);
			appSignUp.controller("ctrCreateurArticle", function($scope, $http, $window) {
				$scope.algorithme = function() {
					var d = {nom : $scope.nomCreationArticle, type : "algorithme"};
			    	var data = JSON.stringify(d);
					$http.post("/ProjetAlgoJEE/CreerArticle", data)
					.then(function(response) {
						if(response.data.retour == "true") {
							$window.location.href = "/ProjetAlgoJEE/OverviewArticle?idArticle=" 
													+ response.data.idArticle
													+ "&type=algorithme";
						} else {
							
						}
					});
				}
			});
			
			$( "#algo_name_input > input" ).focus(function() {
				$("#createurArticle").addClass('active');
			});
			
			$( "#algo_name_input > input" ).focusout(function() {
				$("#createurArticle").removeClass('active');
			});
			</script>
			
			<a id="createurTache" class="header_button" href="/ProjetAlgoJEE/CreationTache">
			<p>
				Créer tache
			</p>
			</a>
 			<!--<div id="createurTache" class="header_button dropdown_button" tabindex="1" ng-controller="ctrCreateurTache">
				<p>Créer tâche</p>
				<button class="header_button">Créer tache</button>
			</div>-->
			<% } %>
	 
		    <script>
			function doSearch() {
				window.location.href="/ProjetAlgoJEE/Recherche?query=" + $("#searchField").val() + "&page=0&compte=10&algorithme=true";
			}
			</script>
			
			<div class="header_form_container">
				<div class="header_form_centerer"></div>
				<div id="search_form">
					<input id="searchField" name="query" type="text" size="40" placeholder="Search..." />
				    <button type="submit" value="Submit"  onclick="doSearch()">Go!</button>
				</div>
				<div class="header_form_centerer"></div>
			</div>		
			
			
			
			
		</div>
	</header>


	<div id="table_of_contents">
	</div>


	<section id="main_content">