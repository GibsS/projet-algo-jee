<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<h4 class="">Recherche</h4>
      
<div class="input-group">
      <input id="searchField" type="text" class="form-control" placeholder="Search for...">
      <span class="input-group-btn">
        <button id="searchButton" class="btn btn-default" type="button" onclick="onClickSearch()">Go!</button>
      </span>
      <script>
	  function onClickSearch() {
			window.location.href="/ProjetAlgoJEE/Recherche?query=" + $("#searchField").val() + "&page=0&compte=10&algorithme=true";
			$("#searchButton").innerHTML = "waiting..";
	  }
      </script>
</div>