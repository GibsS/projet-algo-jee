<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<h4 class="">Creer un article</h4>
      
<div id="createurArticle" ng-controller="ctrCreateurArticle">
 Nom de l'article : <input type="text" ng-model="nomCreationArticle"><br>
 	<div class="dropdown">
  	<button class="btn btn-default" type="button" data-toggle="dropdown" style="width:100%">Type d'article
	 <span class="caret"></span></button>
  	<ul class="dropdown-menu" style="width:100%">
    	<li><a href="" ng-click="algorithme()">Algorithme</a></li>
  	</ul>
	</div>
<script>
var appSignUp = angular.module("createurArticle", []);

appSignUp.controller("ctrCreateurArticle", function($scope, $http, $window) {
	$scope.algorithme = function() {
		var d = {nom : $scope.nomCreationArticle, type : "algorithme"};
    	var data = JSON.stringify(d);
		$http.post("/ProjetAlgoJEE/CreerArticle", data)
		.then(function(response) {
			if(response.data.retour == "true") {
				$window.location.href = "/ProjetAlgoJEE/OverviewArticle?idArticle=" 
										+ response.data.idArticle
										+ "&type=algorithme";
			} else {
				
			}
		});
	}
});
/*angular.bootstrap(document.getElementById("createurArticle"),['createurArticle']);*/
</script>
</div>