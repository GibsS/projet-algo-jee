<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    
    import="java.util.Map"%>

<% 
String url = (String)request.getAttribute("url");
Map<String, String> extraParameter = (Map<String, String>)request.getAttribute("extraParameter");

String[] smallFields = (String[])request.getAttribute("smallFields");
String[] longFields = (String[])request.getAttribute("longFields");
String[] itemLists = (String[])request.getAttribute("itemLists");
String[] codeFields = (String[])request.getAttribute("codeFields");
%>
<button ng-click="submit()">submit</button> {{warning}}
<script>
var smallFields = [
<% if(smallFields != null && smallFields.length >0) {
	out.println("\"" + smallFields[0] + "\"");
	for(int i = 1; i < smallFields.length; i++) {
		out.println(", \"" + smallFields[i] + "\"");
	}
}%>];
var longFields = [
<% if(longFields != null && longFields.length >0) {
	out.println("\"" + longFields[0] + "\"");
	for(int i = 1; i < longFields.length; i++) {
		out.println(", \"" + longFields[i] + "\"");
	}
}%>];
var codeFields = [
<% if(codeFields != null && codeFields.length >0) {
	out.println("\"" + codeFields[0] + "\"");
	for(int i = 1; i < codeFields.length; i++) {
		out.println(", \"" + codeFields[i] + "\"");
	}
}%>];
var itemLists = [
<% if(itemLists != null && itemLists.length >0) {
	out.println("\"" + itemLists[0] + "\"");
	for(int i = 1; i < itemLists.length; i++) {
		out.println(", \"" + itemLists[i] + "\"");
	}
}%>];

var appEditeur = angular.module("editeur", []);

appEditeur.controller("ctrEditeur", function($scope, $http) {
	$scope.warning = "";
	
	for(var i = 0; i < itemLists.length; i++) {
		$scope[itemLists[i] + "Liste"] = [];
		$scope[itemLists[i] + "Query"] = "";
		$scope[itemLists[i] + "Proposition"] = []
	}	
	$scope.recherche = function(nom) {
		var d = {query : $scope[nom + "Query"], algorithme : true};
		var data = JSON.stringify(d);
		$http.post("/ProjetAlgoJEE/RechercheId", data)
		.then(function(response) {
			$scope[nom + "Proposition"] = response.data.resultat;
		});
	}
	$scope.pick = function(nom, article) {
		$scope[nom + "Liste"].push(article);
	}	  
	$scope.unpick = function(nom, article) {
		var index = $scope[nom + "Liste"].indexOf(article);
		if(index > -1)
			$scope[nom + "Liste"].splice(index, 1);
	}
	
	$scope.submit = function() {
    	var d = {};
   	 	for(var i = 0; i < smallFields.length; i++)
   	   	 	d[smallFields[i]] = $scope[smallFields[i]];
   	 	for(var i = 0; i < longFields.length; i++)
	   	 	d[longFields[i]] = CKEDITOR.instances[longFields[i]].getData();//$scope[longFields[i]]; 	
   	 	for(var i = 0; i < codeFields.length; i++)
   	   	 	d[codeFields[i]] = CKEDITOR.instances[longFields[i]].getData();//$scope[codeFields[i]];
	   	for(var i = 0; i < itemLists.length; i++)
		   	d[itemLists[i]] = $scope[itemLists[i]+"Liste"];
	   	<% 	if(extraParameter != null) {
	   		for(String s : extraParameter.keySet()) { 
	   	%>
				<%="d." + s + " = '" + extraParameter.get(s) + "';"%>
	   	<% 
	   		}} 
	   	%>
    	var data = JSON.stringify(d);
		$http.post("<%=url%>", data)
		.then(function(response) {
			if(response.data.retour == "true") {
				console.log("successfull commit");
				window.location.href="/ProjetAlgoJEE/";
			} else {
				console.log("failed commit");
				$scope.warning = "Echec du commit, réessayé plus tard";
			}
		});
    }
});
</script>
</div>