<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<% 
String nom = (String)request.getAttribute("nom"); 
String tmp = nom.substring(0, 1).toUpperCase() + nom.substring(1);
%>
<h2><%=tmp %></h2>
Choix : <small>{{<%=nom %>Liste.length}}</small>
<ul>
<li ng-repeat="article in <%=nom%>Liste"><a href="" ng-click="unpick('<%=nom%>',article)">{{article.nom}}</a></li>
</ul>
<input type="text" ng-model="<%=nom%>Query"> 
<button ng-click="recherche('<%=nom%>')">Rechercher : {{<%=nom %>Query}}</button><br>
Proposition : <small>{{<%=nom%>Proposition.length}}</small>
<ul>
<li ng-repeat="article in <%=nom%>Proposition"><a href="" ng-click="pick('<%=nom %>', article)">{{article.nom}}</a></li>	
</ul>