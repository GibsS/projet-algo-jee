<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<% 
String nom = (String)request.getAttribute("nom"); 
String tmp = nom.substring(0, 1).toUpperCase() + nom.substring(1);
%>
<h2><%=tmp %></h2>
<textarea name="<%=nom %>" id="<%=nom %>" rows="10" cols="80" ng-model="<%= nom%>">
</textarea>
<script>
	CKEDITOR.replace('<%=nom%>');
</script>