<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="BD.Article.*" 
    import="BD.Utilisateur.*"
    import="BD.Article.Algorithme.*"%>

<% 	FacadeArticle fa = (FacadeArticle)request.getAttribute("facadeArticle");
	int idArticle = (int)Integer.parseInt(request.getParameter("idArticle"));
	Article article = fa.getArticle(idArticle); 
	
	String idEditionS = request.getParameter("idEdition");
	int idEdition = -1;
	if(idEditionS != null) {
		idEdition = Integer.parseInt(idEditionS);
	}
	
	String type = request.getParameter("type");
%>

 <jsp:include page="../Template/start.jsp"/>
<jsp:include page="../Section/General/header.jsp"/>

<% if(type.equals("algorithme")) { 
	AlgorithmeImpl ai;
	if(idEdition == -1) {
		ai = (AlgorithmeImpl)((Algorithme)article).getConsultable();
	} else {
		ai = (AlgorithmeImpl)fa.getArticleImpl(idEdition);
	}
	if(ai != null) {
%>

<article>
	<h1><% out.println(article.getNom()); %></h1>
	<% if(idEdition != -1) { %>
	<div id="vote" ng-controller="ctrVote">
		<a id="up_vote" href="" ng-click="upvote()"><img alt="upvote" src="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/images/up_vote.png"/></a>
		<a id="up_vote_ok" href="" style="display:none"><img alt="upvote_ok" src="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/images/up_vote_ok.png"/></a>
		<a id="down_vote" href="" ng-click="downvote()"><img alt="downvote" src="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/images/down_vote.png"/></a>
		<a id="down_vote_ok" href="" style="display:none"><img alt="downvote" src="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/images/down_vote_ok.png"/></a>
		<script>
		var appVote = angular.module("vote", []);
	
		appVote.controller("ctrVote", function($scope, $http) {
			$scope.downvote = function() {
				var d = {idArticle : <%=idArticle%>, idEdition : <%=idEdition%>, positif : false};
	        	var data = JSON.stringify(d);
				$http.post("/ProjetAlgoJEE/NoterEdition", data)
				.then(function(response) {
					if(response.data.retour == "true") {
						console.log("vote positif");
						$( "#down_vote" ).hide();
						$( "#down_vote_ok" ).show();
						$( "#up_vote" ).show();
						$( "#up_vote_ok" ).hide();
					} else {
						console.log("vote negatif");
					}
				});
			}
			$scope.upvote = function() {
				var d = {idArticle : <%=idArticle%>, idEdition :<%=idEdition%>, positif : true};
	        	var data = JSON.stringify(d);
				$http.post("/ProjetAlgoJEE/NoterEdition", data)
				.then(function(response) {
					if(response.data.retour == "true") {
						console.log("vote positif");
						$( "#down_vote" ).show();
						$( "#down_vote_ok" ).hide();
						$( "#up_vote" ).hide();
						$( "#up_vote_ok" ).show();
					} else {
						console.log("vote negatif");
					}
				});
			}
		});
		</script>
	</div>
	<% } %>

	<div style="border-top:1px solid;">
	<h2>Introduction</h2>
	<p><%= ai.getIntroduction()  %></p>
	</div>
	<div style="border-top:1px solid;">
	<h2>Description</h2>
	<p><%= ai.getDescription()  %></p>
	</div>
	<% if(ai.getSousAlgorithme().size() > 0) { %>
	<div style="border-top:1px solid;">
	<h2>Sous algorithme :</h2>
		<ul>
		<%for(Algorithme a : ai.getSousAlgorithme()) { 
		   String urlConsultation = "/ProjetAlgoJEE/ConsultationArticle?idArticle=" 
   							+ a.getId() 
   							+ "&mode=consultation"
   							+ "&type=" + type;
   							%>
		<li><a href="<%=urlConsultation%>" style="text-decoration: none;"><%= a.getNom() %></a></li>
		<% } %>
		</ul>
	</div>
	<% } %>
<% } else { %>
<p>
Aucune version de consultation n'est disponible pour cet article.<br>
<a href=<%= "/ProjetAlgoJEE/OverviewArticle?idArticle=" + article.getId() + "&type=algorithme"%> style="text-decoration: none;">overview</a>
</p>
<% }} %>
</article>
	
	<% 
	if(idEdition == -1) {
		request.setAttribute("apps", new String[]{"createurArticle"}); 
	} else {
		request.setAttribute("apps", new String[]{"vote", "createurArticle"}); 
	}
	%>





	<jsp:include page="../Section/General/footer.jsp"/>

	<jsp:include page="../Template/end.jsp"/>

