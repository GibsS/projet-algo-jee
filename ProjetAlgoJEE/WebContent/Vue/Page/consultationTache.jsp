<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    
    import="BD.Utilisateur.*"
    import="BD.Article.*"
    import="BD.Article.Algorithme.*"%>

<% int idTache = Integer.parseInt(request.getParameter("idTache"));
	FacadeUtilisateur fu = (FacadeUtilisateur)request.getAttribute("facadeUtilisateur");
	
	Tache tache = fu.getTache(idTache);
%>



    
    <jsp:include page="../Template/start.jsp"/>
   	
   	<jsp:include page="../Section/General/header.jsp"/>
   	
   	

<article>
	<h1><%= tache.getNom() %></h1>
	<% switch(tache.getEtat()) { 
		case Tache.ready : out.println("état : posé, non récupéré");break;
		case Tache.inProgress : out.println("état : en cours"); break;
		case Tache.done : out.println("état : fini"); break;
		case Tache.archived : out.println("état : archivé"); break;
	} %>
	<div id="vote" ng-controller="ctrVote">
		<a id="up_vote" href="" ng-click="upvote()"><img alt="upvote" src="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/images/up_vote.png"/></a>
		<a id="up_vote_ok" href="" style="display:none"><img alt="upvote_ok" src="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/images/up_vote_ok.png"/></a>
		<a id="down_vote" href="" ng-click="downvote()"><img alt="downvote" src="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/images/down_vote.png"/></a>
		<a id="down_vote_ok" href="" style="display:none"><img alt="downvote" src="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/images/down_vote_ok.png"/></a>
		<script>
		var appVote = angular.module("vote", []);
	
		appVote.controller("ctrVote", function($scope, $http) {
			$scope.downvote = function() {
				var d = {idTache : <%=idTache%>, positif : false};
	        	var data = JSON.stringify(d);
				$http.post("/ProjetAlgoJEE/NoterTache", data)
				.then(function(response) {
					if(response.data.retour == "true") {
						console.log("vote positif");
						$( "#down_vote" ).hide();
						$( "#down_vote_ok" ).show();
						$( "#up_vote" ).show();
						$( "#up_vote_ok" ).hide();
					} else {
						console.log("vote negatif");
					}
				});
			}
			$scope.upvote = function() {
				var d = {idTache : <%=idTache%>, positif : false};
	        	var data = JSON.stringify(d);
				$http.post("/ProjetAlgoJEE/NoterTache", data)
				.then(function(response) {
					if(response.data.retour == "true") {
						console.log("vote positif");
						$( "#down_vote" ).show();
						$( "#down_vote_ok" ).hide();
						$( "#up_vote" ).hide();
						$( "#up_vote_ok" ).show();
					} else {
						console.log("vote negatif");
					}
				});
			}
		});
		</script>
	</div>
	<h2>Description :</h2>
	<p><%= tache.getDescription() %></p>
	<h2>Article associés :</h2>
	<ul>
	<% for(Article a : tache.getArticle()) { 
		String urlConsultation = null;
		if(a instanceof Algorithme) {
			urlConsultation = "/ProjetAlgoJEE/ConsultationArticle?idArticle=" 
					+ a.getId() 
					+ "&mode=consultation"
					+ "&type=algorithme";
		}%>
		<li><a href="<%=urlConsultation%>"><%= a.getNom() %></a></li>
	<% } %>
	</ul>
</article>

<% 
if(tache.getEtat() == Tache.done) {
	request.setAttribute("apps", new String[]{"vote", "createurArticle"});
} else {
	request.setAttribute("apps", new String[]{"createurArticle"});
}%>







	<jsp:include page="../Section/General/footer.jsp"/>

	<jsp:include page="../Template/end.jsp"/>