<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    
    import="BD.Utilisateur.*"%>
   
<% Utilisateur util = (Utilisateur)session.getAttribute("util");  %>   
    
<jsp:include page="../Template/start.jsp"/>
   	
<jsp:include page="../Section/General/header.jsp"/>
   	
<article>
	<h1>Bienvenue sur notre petit site</h1>

	<% if(util == null) { %>
	<!-- Application d'inscription -->
	<div id="signUp" ng-controller="ctrSignUp">
		<h2>Sign up</h2>
		<p ng-hide="section">
		<input class="password_input" type="text" placeholder="Username" ng-model="pseudo"></input><br>
		<input class="password_input" type="password" placeholder="Password" ng-model="password"></input><br>
		<button class="log_button" ng-click="submit()">Créer un compte</button>
		</p>
		<p ng-hide="sectionSucces">
		Compte créé
		</p>
		<p ng-hide="sectionEchec">
		Echec
		</p>
		
		<script>
		var appSignUp = angular.module("signUp", []);
	
		appSignUp.controller("ctrSignUp", function($scope, $http) {
			$scope.section = false;
			$scope.sectionSucces = true;
			$scope.sectionEchec = true;
			
	    	$scope.submit = function() {
	        	var d = {pseudo : $scope.pseudo, password : $scope.password};
	        	var data = JSON.stringify(d);
				$http.post("/ProjetAlgoJEE/CreerCompte", data)
				.then(function(response) {
					if(response.data.retour == "true") {
						$scope.section = true;
						$scope.sectionSucces = false;
						$scope.sectionEchec = true;
					} else {
						$scope.section = false;
						$scope.sectionSucces = true;
						$scope.sectionEchec = false;
					}
				});
	        }
		});
		</script>
	</div>
	<% } %>
	
	<div style="border-top:1px solid;">
	<p>
		MustAlgo est un petit site sur l'algorithmie : vous pouvez y trouvez des implémentations d'algorithmie dans plusieurs langages.
	</p>
	<p>
		Vous pouvez aussi rajoutez vos propres implémentations et participer à la vie du site.
	</p>
	</div>
</article>

<%  
if(util == null) { 
	request.setAttribute("apps", new String[]{"signUp", "createurArticle"}); 
} else {
	request.setAttribute("apps", new String[]{"createurArticle"}); 
} 
%>

<jsp:include page="../Section/General/footer.jsp"/>

<jsp:include page="../Template/end.jsp"/>


