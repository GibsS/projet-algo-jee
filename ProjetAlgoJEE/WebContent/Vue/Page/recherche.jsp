<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    
    import="BD.Utilisateur.*"
    import="BD.Article.*"
    import="BD.Article.Algorithme.*"
    import="java.util.List"%>
    
 <% FacadeArticle fa = (FacadeArticle)request.getAttribute("facadeArticle");
 	
 	int pageNum = Integer.parseInt(request.getParameter("page"));
 	int compte = Integer.parseInt(request.getParameter("compte"));
 	String recherche = request.getParameter("query");
 	if(recherche == null)
 		recherche = "";
 	boolean algorithme = Boolean.parseBoolean(request.getParameter("algorithme"));
 	
 	List<Article> articles = fa.getArticles(recherche, algorithme);%>   

<jsp:include page="../Template/start.jsp"/>
<jsp:include page="../Section/General/header.jsp"/>
   	
<article>
<h1>Recherche <small><%= articles.size() %> trouvé</small></h1>
<script>
function doArticleSearch() {
	window.location.href="/ProjetAlgoJEE/Recherche?query=" + $("#articleSearchField").val() + "&page=0&compte=10&algorithme=true";
}
</script>
<input type="text" id="articleSearchField"/>
<input type="submit" onclick="doArticleSearch()" name="chercher"/>
<% for(Article article : articles) { 
	if(article instanceof Algorithme) {
		Algorithme algo = (Algorithme)article;
		String urlConsultation = "/ProjetAlgoJEE/ConsultationArticle?idArticle=" + article.getId() + "&mode=consultation&type=algorithme";
		String urlOverview = "/ProjetAlgoJEE/OverviewArticle?idArticle=" + article.getId() + "&type=algorithme";
%>
		<h2 style="border-top:1px solid;"> <a href=<%=urlConsultation%> style="text-decoration: none;"><%= article.getNom() %></a></h2>
		<p>
		<% AlgorithmeImpl ai = ((AlgorithmeImpl)((Algorithme)article).getConsultable());
		   if(ai != null) {
			String texte = ((AlgorithmeImpl)((Algorithme)article).getConsultable()).getIntroduction();
			
			if(texte.length() <= 200) {
				out.println(texte);
			} else {
				out.println(texte.substring(0, 200) + "...");
			}
			%> 
		<% } else { %>
			Cette algorithme n'a pas encore de version consultable.
		<% } %>
		</p>
		<a href=<%= urlOverview %> style="text-decoration: none;">overview</a>
	<% } %>
<% } %>
</article>

<% request.setAttribute("apps", new String[]{"createurArticle"}); %>
<jsp:include page="../Section/General/footer.jsp"/>
<jsp:include page="../Template/end.jsp"/>