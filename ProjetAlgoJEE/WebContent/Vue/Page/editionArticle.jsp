<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="BD.Article.*" 
    import="BD.Utilisateur.*"
    import="BD.Article.Algorithme.*"
    import="java.util.Map"
    import="java.util.HashMap"%>
    
<%  FacadeArticle fa = (FacadeArticle)request.getAttribute("facadeArticle");

	int idArticle = Integer.parseInt(request.getParameter("idArticle"));
	Article article = fa.getArticle(idArticle);
	
	String idEditionS = request.getParameter("idEdition");
	int idEdition = -1;
	ArticleImpl ai = null;
	if(idEditionS != null) {
		idEdition = Integer.parseInt(idEditionS);
		ai = fa.getArticleImpl(idEdition);
	}
	
	String type = request.getParameter("type");
%>

<jsp:include page="../Template/start.jsp"/>
<jsp:include page="../Section/General/header.jsp"/>

<article>
	<h1>Edition</h1>
	<div class="row">
		<% if(type.equals("algorithme")) {
			Algorithme algo = (Algorithme)article;
			//AlgorithmeImpl algoImpl = (AlgorithmeImpl)ai;
		%>
			
		<jsp:include page="../Section/Editeur/startEditeur.jsp"/>
			
		<% request.setAttribute("nom", "introduction"); %>
		<jsp:include page="../Section/Editeur/longTextField.jsp"/>
		<% request.setAttribute("nom", "description"); %>
		<jsp:include page="../Section/Editeur/longTextField.jsp"/>
		<% request.setAttribute("nom", "sousAlgorithme"); %>
		<jsp:include page="../Section/Editeur/itemList.jsp"/>
			
		<% 
		request.setAttribute("url", "/ProjetAlgoJEE/EditerArticle"); 
		Map<String, String> param = new HashMap<String, String>();
		param.put("type", "algorithme");
		if(idEdition > 0) {
			param.put("idEdition", Integer.toString(idEdition));
		}
		param.put("idArticle", Integer.toString(algo.getId()));
		request.setAttribute("extraParameter", param);
		
		String[] smallFields = null;
		String[] longFields = new String[]{"introduction", "description"};
		String[] itemLists = new String[]{"sousAlgorithme"};
		String[] codeFields = null;
		request.setAttribute("smallFields", smallFields);
		request.setAttribute("longFields", longFields);
		request.setAttribute("itemLists", itemLists);
		request.setAttribute("codeFields", codeFields);
		%>
		<jsp:include page="../Section/Editeur/endEditeur.jsp"/>
		<% } %>
	</div>
</article>

<% request.setAttribute("apps", new String[]{"editeur", "createurArticle"}); %>
<jsp:include page="../Section/General/footer.jsp"/>
<jsp:include page="../Template/end.jsp"/>