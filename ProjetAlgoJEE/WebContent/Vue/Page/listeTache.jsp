<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    
    import="BD.Utilisateur.FacadeUtilisateur"
    import="BD.Utilisateur.Tache"
    import="BD.Article.*"%>

<%
FacadeUtilisateur fu = (FacadeUtilisateur)request.getAttribute("facadeUtilisateur");
%>


    <jsp:include page="../Template/start.jsp"/>
   	
   	<jsp:include page="../Section/General/header.jsp"/>
   	
   	
<article>
<h1>Taches :</h1>
<% for(Tache t : fu.getTaches()) { %>
	<h2><%= t.getNom() %></h2>
	<%=t.getDescription() %>
	<ul>
	<%for(Article a : t.getArticle()) { %>
		<li><%=a.getNom() %></li>
	<% } %>
	</ul>
<% } %>
</article>

<% request.setAttribute("apps", new String[]{"createurArticle"}); %>





	<jsp:include page="../Section/General/footer.jsp"/>

	<jsp:include page="../Template/end.jsp"/>