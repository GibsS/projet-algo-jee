<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    
    import="BD.Utilisateur.*"
    import="BD.Article.*"%>

<% FacadeUtilisateur fu = (FacadeUtilisateur)request.getAttribute("facadeUtilisateur");
   FacadeArticle fa = (FacadeArticle)request.getAttribute("facadeArticle");
   
   int idArticle = Integer.parseInt(request.getParameter("idArticle"));  
   Article article = fa.getArticle(idArticle);
   
   String type = request.getParameter("type");
   
   String urlConsultation = "/ProjetAlgoJEE/ConsultationArticle?idArticle=" 
   							+ idArticle 
   							+ "&mode=consultation"
   							+ "&type=" + type;%>
   							
<jsp:include page="../Template/start.jsp"/>
<jsp:include page="../Section/General/header.jsp"/>

<% if(type.equals("algorithme")) { %>
	<article>
		<h1><a href="<%=urlConsultation%>" style="text-decoration: none;"><%=article.getNom()%></a></h1>
		<h2>Edition <small><%= article.getEdition().size() %> trouvé</small></h2>
		<a href=<%= "/ProjetAlgoJEE/EditionArticle?idArticle=" + article.getId() + "&type=algorithme"%> style="text-decoration: none;">Créer une nouvelle édition</a>
		<% for(ArticleImpl ai : article.getEdition()) {%>
 			<% String urlEdition = "/ProjetAlgoJEE/ConsultationArticle?idArticle=" + idArticle + "&mode=edition&idEdition=" + ai.getId() + "&type=algorithme"; %>
 			<h3><a href="<%= urlEdition %>" style="text-decoration: none;">Edition de : <%= ai.getCreateur() %></a></h3>
 			<p><%= ai.getCommentaire() %></p>
		<% } %>
		<h2>Tache <small><%= article.getRequetesTache().size() %> trouvée</small></h2>
		<% for(Tache t : article.getRequetesTache()) {%>
	 			<% String urlEdition = "/ProjetAlgoJEE/ConsultationTache?idTache=" + t.getId(); %>
 			<h3><a href="<%= urlEdition %>" style="text-decoration: none;"><%= t.getNom() %></a></h3>
 			<p><%= t.getDescription() %></p> 
		<% } %>
	</article>
<% } %>

<% request.setAttribute("apps", new String[]{"createurArticle"}); %>
<jsp:include page="../Section/General/footer.jsp"/>
<jsp:include page="../Template/end.jsp"/>