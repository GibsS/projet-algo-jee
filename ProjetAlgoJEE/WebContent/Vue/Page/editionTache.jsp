<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<jsp:include page="../Template/start.jsp"/>
<jsp:include page="../Section/General/header.jsp"/>
   	
<article>
	<h1>Créer une tache</h1>

	<jsp:include page="../Section/Editeur/startEditeur.jsp" />
	
	<% request.setAttribute("nom", "nom"); %>
	<jsp:include page="../Section/Editeur/smallTextField.jsp" />
	<% request.setAttribute("nom", "description"); %>
	<jsp:include page="../Section/Editeur/longTextField.jsp" />
	<% request.setAttribute("nom", "article"); %>
	<jsp:include page="../Section/Editeur/itemList.jsp" />
	
	<% 
	request.setAttribute("url", "/ProjetAlgoJEE/CreerTache"); 
	
	String[] smallFields = new String[]{"nom"};
	String[] longFields = new String[]{"description"};
	String[] itemLists = new String[]{"article"};
	String[] codeFields = null;
	request.setAttribute("smallFields", smallFields);
	request.setAttribute("longFields", longFields);
	request.setAttribute("itemLists", itemLists);
	request.setAttribute("codeFields", codeFields);
	%>
	<jsp:include page="../Section/Editeur/endEditeur.jsp" />
</article>


<% request.setAttribute("apps", new String[]{"editeur", "createurArticle"}); %>

<jsp:include page="../Section/General/footer.jsp"/>
<jsp:include page="../Template/end.jsp"/>