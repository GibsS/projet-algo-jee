<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"    
    
    import="BD.Utilisateur.*"
    import="BD.Article.*"%>
    
<% 
int idUtil = Integer.parseInt(request.getParameter("idUtil"));
FacadeUtilisateur fu = (FacadeUtilisateur)request.getAttribute("facadeUtilisateur");
FacadeArticle fa = (FacadeArticle)request.getAttribute("facadeArticle");
Utilisateur utilisateur = fu.getUtilisateur(idUtil);
%>
   
<jsp:include page="../Template/start.jsp"/>
<jsp:include page="../Section/General/header.jsp"/>
   	
<article>
	<h1><%= utilisateur.getPseudo()%> <small><%= fu.getScore(utilisateur) %></small></h1>
</article>

<% request.setAttribute("apps", new String[]{"createurArticle"}); %>
<jsp:include page="../Section/General/footer.jsp"/>
<jsp:include page="../Template/end.jsp"/>