<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Mustalgo</title>
    <meta name="description" content="LE site des algos." />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=yes" />

	<!-- Favicons -->
	<link rel="apple-touch-icon" sizes="57x57" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/manifest.json">
	<link rel="mask-icon" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- Stylesheet -->
	<link rel="stylesheet" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/css/style.css" type="text/css" media="screen and (min-width: 900px)">
	<link rel="stylesheet" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/css/style_min.css" type="text/css" media="screen and (max-width: 900px)">
	<link rel="stylesheet" href="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/css/jquery.tocify.css" type="text/css"/>
	
	<!--  Some Javascript -->
	<!-- JQuery -->
	<script src="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/js/jquery-2.2.0.min.js"></script>
	
	<!-- AngularJS -->
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

	<!-- CK Editor -->
	<script type="text/javascript" src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>

</head>

<body>