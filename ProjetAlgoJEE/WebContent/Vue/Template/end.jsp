<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<% String[] apps = (String[])request.getAttribute("apps"); 
if(apps != null) {%>
<script>
angular.element(document).ready(function() {
	<% for(String app : apps) { %>
		angular.bootstrap(document.getElementById("<%= app %>"),['<%= app %>']);
	<% } %>
});
</script>
<% } %> 
	


	<!-- Search launch script -->
	<script>
		function onClickSearch() {
			window.location.href="/ProjetAlgoJEE/Recherche?query=" + $("#searchField").val();
			$("#searchButton").innerHTML = "waiting..";
		}
	</script>

	<!-- Dynamic Table of content -->
    <script src="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/js/jquery-ui.min.js"></script>
    <script src="http://www.bde.enseeiht.fr/~metaisp/ProjetJEE/theme/js/jquery.tocify.min.js"></script>
    <script type="text/javascript">
    //Executes your code when the DOM is ready.  Acts the same as $(document).ready().
	$(function() {
	  // Calls the selectBoxIt method on your HTML select box and updates the showEffect option
	  var toc = $("#table_of_contents").tocify().data("toc-tocify");

	  // Sets the showEffect, scrollTo, and smoothScroll options
	  toc.setOptions({ context: $("#main_content > article"), theme: "none", extendPage: false, scrollTo: 70, smoothScrollSpeed: "slow", history: false });
	});
    </script>

    <!-- Scroll-up button -->
    <script type="text/javascript">
	$(function () {

	    $(window).scroll(function () {
	        if ($(this).scrollTop() > 100) {
	            $('.scrollup').fadeIn();
	        } else {
	            $('.scrollup').fadeOut();
	        }
	    });

	    $('.scrollup').click(function () {
	        $("html, body").animate({
	            scrollTop: 0
	        }, 600);
	        return false;
	    });

	});
    </script>
</body>
</html>